#--------------------------------------------------------------
#
# Source code of Collie River Basin 2 hydrological model
#
# Author: Yohei Sawada
#
#---------------------------------------------------------------


from pylab import *
from scipy import *
import numpy as np
import matplotlib.pyplot as plt
import yaml

# model parameters
Smax = 1000 # maximum soil moisture storage [mm] [1-2000]
Sfc = 0.50*Smax # Field capacity [mm] [0.05-0.95]
a = 0.5 # runoff coefficient [d^-1] [0-1]
M = 0.7 # Forest fraction [-] [0.05-0.95]

# Other Configuration
Tend = 10 # Total timesteps [d]

# Initializing state variables
S = np.zeros((Tend+1)) #Storage [mm]
S[:] = 1200.0

print('state variable = ', S)

# reading input variables from MERV-Jp
filename = '../varssim086.csv'
data = np.loadtxt(filename,delimiter=',',skiprows=1)
P = data[0:4018,4] #precipitation [mm/d]
Ep = data[0:4018,6] #potential evapotranspiration [mm/d]

# visualizing P & Ep ----------------------------
#plt.plot(P[0:365],label='precipitation')
#plt.plot(Ep[0:365],label='potential evapotranspiration')
#plt.legend()
#plt.show()
# visualizing P & Ep ----------------------------

# initializing main exacutable code for the model
# Calculating fluxes
t = 5
Eb = Ep[t]*(1-M)*S[t]/Smax # Evaporation from soil
if S[t] > Sfc:
    Ev = M*Ep[t] # transpiration from vegetation
    Qss = a*(S[t]-Sfc) # Subsurface runoff
else:
    Ev = M*Ep[t]*S[t]/Sfc # transpiration from vegetation
    Qss = 0.0 # subsurface runoff

if S[t] > Smax:
    Qse = P[t] # saturation excess overland flow
else:
    Qse = 0.0 # no saturation excess overland flow

print('P = ', P[t])
print('Ep = ', Ep[t])
print('Eb = ', Eb)
print('Ev = ', Ev)
print('Qss = ', Qss)
print('Qse = ', Qse)

