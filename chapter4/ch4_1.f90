!
!Fortran verion of Collie River Basin model 2
!
!Developed by Yohei Sawada
!
!in this version, parameters are hard-coded
!
program Collie_River_Basin_model2

implicit none

!model parameters
real, parameter:: Smax = 1000 ! maximum soil moisture storage [mm] [1-2000]
real, parameter:: Sfc_fraction = 0.50 ! Field capacity (need to multiply Smax) [mm] [0.05-0.95]
real:: Sfc
real, parameter:: a = 0.50 ! runoff coefficient [d^(-1)] [0-1]
real, parameter:: M = 0.7 ! Forest fraction [-] [0.05-0.95]


!other configulation
integer, parameter:: Tend = 365 ! total timesteps [d]

! state variables
real:: S(Tend+1) ! soil moisture storage

! input variables
real:: P(Tend) ! precipitation
real:: Ep(Tend) ! potential evapotranspiration
real:: obsQ(Tend), referenceQ(Tend) ! observed runoff & reference runoff in MERV-Jp


! Fluxes
real:: Eb, Ev !soil evaporation & vegetation transpiration
real:: Qss, Qse, Q(Tend) ! subsurface runoff, saturation excess overland flow, total runoff

! Others
character(30):: filename
real:: data_merv(Tend,52)
real:: NSEsim, NSEref
integer:: t

! end variable decleration
! start executable code

Sfc = Sfc_fraction * Smax

!
! reading input file
!
filename = '../varssim086.csv'
open (20, file=trim(filename), status='old')
read (20,*) !Skip header
do t = 1, Tend
 read(20,*) data_merv(t,:)
enddo
close(20)
P = data_merv(:,5) ! precipitation
Ep = data_merv(:,7) ! potential evapotranspiration

!
! Initializing model state variables
!
S(1) = 100.0

!
! Main code for the model
!
do t = 1, Tend
 !
 ! calculating fluxes
 !
 Eb = Ep(t)*(1-M)*S(t)/Smax ! evaporation from soil
 if (S(t) > Sfc) then
         Ev = M*Ep(t) ! transpiration from vegetation
         Qss = a*(S(t)-Sfc) ! subsurface runoff
 else
         Ev = M*Ep(t)*S(t)/Sfc ! transpiration from vegetation
         Qss = 0.0 ! subsurface runoff
 endif

 if (S(t) > Smax) then
         Qse = P(t) ! saturation excess overland flow
 else
         Qse = 0.0 ! no saturation excess overland flow
 endif

 Q(t) = Qss + Qse !total runoff

 !
 ! Updating storage
 !
 S(t+1) = S(t) + P(t) - Eb - Ev - Qse - Qss

 ! monitor
 print *, 'integrating at time ', t
 print *, 'rainfall, storage, runoff ', P(t), S(t), Q(t)

enddo ! end t

!
! Writing output Q(t)
!
open (21, file='output.txt',status='unknown')
do t = 1, Tend
 write(21,*) Q(t)
enddo

!
! reading observed & reference runoff
!
obsQ = data_merv(:,8)
referenceQ = data_merv(:,11) 

!
! Calculate NSE
!
call NSE(Q, obsQ, Tend, NSEsim)
call NSE(referenceQ, obsQ, Tend, NSEref)

print *, 'NSEs = ', NSEsim, NSEref

end program

!
! Subroutine NSE
! Nash-Sutcliffe model Efficiency coefficiency (NSE)
!
subroutine NSE(f, o, T, score)

implicit none

integer, intent(in):: T
real, intent(in):: f(T), o(T)
real, intent(out):: score
real:: numerator, denominator, clim
integer :: i

numerator = 0.0
clim = 0.0
denominator = 0.0
do i = 1, T
 if (o(i) >= 0.0) then
  numerator = numerator + ((f(i) - o(i))**2.0)/T
  clim = clim + o(i)/T
 endif
enddo
do i = 1, T
 if (o(i) >= 0.0) then
  denominator = denominator + ((o(i) - clim)**2.0)/T
 endif
enddo

score = 1 - (numerator/denominator)

end subroutine




