!
!Fortran verion of Collie River Basin model 2
!
!Developed by Yohei Sawada
!
!in this version, parameters are hard-coded
!
program ParaSearch_Collie_River_Basin_model2

implicit none

!model parameters
real:: Smax, Smax_min=1.0, Smax_max=2000.0 ! maximum soil moisture storage [mm] [1-2000]
real:: Sfc_fraction, Sfc_fraction_min=0.05, Sfc_fraction_max=0.95 ! Field capacity (need to multiply Smax) [mm] [0.05-0.95]
real:: Sfc
real:: a, a_min=0.0, a_max=1.0 ! runoff coefficient [d^(-1)] [0-1]
real:: M, M_min=0.05, M_max=0.95 ! Forest fraction [-] [0.05-0.95]
integer, parameter:: inc=20

!other configulation
integer, parameter:: Tend = 4017 ! total timesteps [d]

! state variables
real:: S(Tend+1) ! soil moisture storage

! input variables
real:: P(Tend) ! precipitation
real:: Ep(Tend) ! potential evapotranspiration
real:: obsQ(Tend), referenceQ(Tend) ! observed runoff & reference runoff in MERV-Jp


! Fluxes
real:: Q(Tend) ! subsurface runoff, saturation excess overland flow, total runoff

! Others
character(30):: filename
real:: data_merv(Tend,52)
real:: NSEsim(inc,inc,inc,inc)
integer:: i,j,k,l

! end variable decleration
! start executable code


!
! reading input file
!
filename = '../varssim086.csv'
open (20, file=trim(filename), status='old')
read (20,*) !Skip header
do i = 1, Tend
 read(20,*) data_merv(i,:)
enddo
close(20)
P = data_merv(:,5) ! precipitation
Ep = data_merv(:,7) ! potential evapotranspiration
obsQ = data_merv(:,8)


!
! Initializing model state variables
!
S(1) = 100.0

!
! Main code for the model
!
!
! Parameters are varied
!
do i = 1, inc
 Smax = Smax_min + real(i) * (Smax_max-Smax_min)/real(inc)
 do j = 1, inc
  Sfc = Smax * (Sfc_fraction_min + real(j) * (Sfc_fraction_max-Sfc_fraction_min)/real(inc))
  do k = 1, inc
   a = a_min + real(k)* (a_max-a_min)/real(inc)
   do l = 1, inc
    M = M_min + real(l) * (M_max-M_min)/real(inc)
    !
    ! Initialize
    !
    S = 0.0
    S(1) = 100.0
    Q = 0.0
    !
    ! Main conceptual hydrological model
    !
    call hydromodel(Smax, Sfc, a, M, P, Ep, S, Q, Tend)
    call NSE(Q, obsQ, Tend, NSEsim(l,k,j,i))
    print *, 'integrating at setting, ', i,j,k,l
    print *, 'parameters', Smax, Sfc, a, M
    print *, 'NSE = ', NSEsim(l,k,j,i)
   enddo ! end l
  enddo ! end k
 enddo ! end j
enddo ! end i
!
! Writing output Q(t)
!
open (21, file='output.txt',status='unknown')
write(21,*) NSEsim

end program



subroutine hydromodel(Smax, Sfc, a, M, P, Ep, S, Q, Tend)

implicit none
integer, intent(in):: Tend
real, intent(in):: Smax, Sfc, a, M, P(Tend), Ep(Tend)
real, intent(inout):: S(Tend), Q(Tend)
integer:: t
! Fluxes
real:: Eb, Ev !soil evaporation & vegetation transpiration
real:: Qss, Qse ! subsurface runoff, saturation excess overland flow, total runoff

do t = 1, Tend
 !
 ! calculating fluxes
 !
 Eb = Ep(t)*(1-M)*S(t)/Smax ! evaporation from soil
 if (S(t) > Sfc) then
         Ev = M*Ep(t) ! transpiration from vegetation
         Qss = a*(S(t)-Sfc) ! subsurface runoff
 else
         Ev = M*Ep(t)*S(t)/Sfc ! transpiration from vegetation
         Qss = 0.0 ! subsurface runoff
 endif

 if (S(t) > Smax) then
         Qse = P(t) ! saturation excess overland flow
 else
         Qse = 0.0 ! no saturation excess overland flow
 endif

 Q(t) = Qss + Qse !total runoff

 !
 ! Updating storage
 !
 S(t+1) = S(t) + P(t) - Eb - Ev - Qse - Qss

 ! monitor
 !print *, 'integrating at time ', t
 !print *, 'rainfall, storage, runoff ', P(t), S(t), Q(t)

enddo ! end t

end subroutine

!
! Subroutine NSE
! Nash-Sutcliffe model Efficiency coefficiency (NSE)
!
subroutine NSE(f, o, T, score)

implicit none

integer, intent(in):: T
real, intent(in):: f(T), o(T)
real, intent(out):: score
real:: numerator, denominator, clim
integer :: i

numerator = 0.0
clim = 0.0
denominator = 0.0
do i = 1, T
 if (o(i) >= 0.0) then
  numerator = numerator + ((f(i) - o(i))**2.0)/T
  clim = clim + o(i)/T
 endif
enddo
do i = 1, T
 if (o(i) >= 0.0) then
  denominator = denominator + ((o(i) - clim)**2.0)/T
 endif
enddo

score = 1 - (numerator/denominator)

end subroutine




