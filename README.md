# Programming Training Camp

Source code can be found for Sawada Lab's programming training camp.
The virtual textbook can be found at https://www.notion.so/Sawada-Lab-Programming-training-camp-aaed763c517e4da6afc00de991af0ce3

This source code and virtual textbooks were originally developed for the internal seminar in Sawada Group.
It cannot be guaranteed that the source codes can work in the other environment and context.

Sawada Group HP: https://sites.google.com/view/sawadaresearchgroup/home
