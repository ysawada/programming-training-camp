#--------------------------------------------------------------
#
# Source code of Collie River Basin 2 hydrological model
#
# Author: Yohei Sawada
#
#---------------------------------------------------------------


from pylab import *
from scipy import *
import numpy as np
import matplotlib.pyplot as plt
import yaml

def NSE(f,o): #Nash-Sutcliffe Model Efficiency Coefficient
    clim = np.ones((len(o)))*np.nanmean(o)
    numerator = np.nansum((f-o)**2)
    denominator = np.nansum((o-clim)**2)
    return 1 - (numerator/denominator)

# reading configuration file
filename = 'config.yml'
with open(filename,'r') as ymlfile:
    params = yaml.full_load(ymlfile)

# parameters
Smax = params['parameters']['Smax']
Sfc = params['parameters']['Sfc'] * Smax
a = params['parameters']['a']
M = params['parameters']['M']

# other configulations
Tend = params['TimePeriod']['Tend']

# Initializing state variables
S = np.zeros((Tend+1)) #Storage [mm]
S[0] = params['InitialConditions']['Storage']

# Input meteorological forcings
filename = params['MERV-JpDATA']['infile']
data = np.loadtxt(filename,delimiter=',',skiprows=1)

P = data[0:4018,4] #precipitation [mm/d]
Ep = data[0:4018,6] #potential evapotranspiration [mm/d]

# output
Q = np.zeros((Tend))

# initializing main exacutable code for the model
for t in range(0,Tend):
    # Calculating fluxes
    Eb = Ep[t]*(1-M)*S[t]/Smax # Evaporation from soil
    if S[t] > Sfc:
        Ev = M*Ep[t] # transpiration from vegetation
        Qss = a*(S[t]-Sfc) # Subsurface runoff
    else:
        Ev = M*Ep[t]*S[t]/Sfc # transpiration from vegetation
        Qss = 0.0 # subsurface runoff

    if S[t] > Smax:
        Qse = P[t] # saturation excess overland flow
    else:
        Qse = 0.0 # no saturation excess overland flow

    Q[t] = Qss + Qse # total runoff

    # Updating Storage
    S[t+1] = S[t] + P[t] - Eb - Ev - Qse - Qss

    # Monitor
    print('integrating at time', t)
    print('rainfall, storage, runoff = ', P[t], S[t], Q[t])

# writing output Q[t] in txt file
np.savetxt('output.txt',Q)

# observed runoff
obsQ = data[0:4018,7]
obsQ[obsQ<0] = nan

# well-calibrated model
referenceQ = data[0:4018,10]

plt.plot(obsQ[0:Tend],label='observation')
plt.plot(Q[0:Tend],label='my simulation')
plt.plot(referenceQ[0:Tend],label='reference in MERV-Jp')
plt.legend()
plt.show()

NSEsim = NSE(Q[0:Tend],obsQ[0:Tend])
NSEref = NSE(referenceQ[0:Tend],obsQ[0:Tend])

print('NSEs for your simulation and reference in MERV-Jp are ', NSEsim, NSEref)

