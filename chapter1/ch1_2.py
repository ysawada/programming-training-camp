#--------------------------------------------------------------
#
# Source code of Collie River Basin 2 hydrological model
#
# Author: Yohei Sawada
#
#---------------------------------------------------------------


from pylab import *
from scipy import *
import numpy as np
import matplotlib.pyplot as plt
import yaml

# model parameters
Smax = 1000 # maximum soil moisture storage [mm] [1-2000]
Sfc = 0.50*Smax # Field capacity [mm] [0.05-0.95]
a = 0.5 # runoff coefficient [d^-1] [0-1]
M = 0.7 # Forest fraction [-] [0.05-0.95]

# Other Configuration
Tend = 10 # Total timesteps [d]

# Initializing state variables
S = np.zeros((Tend+1)) #Storage [mm]
S[0] = 100.0

print('state variable = ', S)


